package com.booklibrary.BOOKLIBRARY.repository;


import com.booklibrary.BOOKLIBRARY.entity.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PublisherRepository extends JpaRepository<Publisher, Long> {

}
