package com.booklibrary.BOOKLIBRARY.repository;


import com.booklibrary.BOOKLIBRARY.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {

}
