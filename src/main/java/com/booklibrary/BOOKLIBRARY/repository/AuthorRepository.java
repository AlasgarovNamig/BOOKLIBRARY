package com.booklibrary.BOOKLIBRARY.repository;


import com.booklibrary.BOOKLIBRARY.entity.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Long> {

}
